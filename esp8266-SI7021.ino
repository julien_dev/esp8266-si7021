#include <ESP8266WiFi.h>
#include <Wire.h>

#ifdef ESP8266
extern "C" {
#include "user_interface.h"
}
#endif

 
String apiKey = "";   // remplacer par votre chaine thingspeak (API key),
const char* ssid = "";     // remplacer par le SSID de votre WiFi
const char* password = "";  // remplacer par le mot de passe de votre WiFi
const char* server = "api.thingspeak.com";

const int sleepTimeS = 10;

// SI7021 I2C address is 0x40(64)
#define si7021Addr 0x40     

WiFiClient client;

void ConnectionWifi(){
  WiFi.begin(ssid, password);
  
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
   
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
}

void setup() {                
  Serial.begin(115200);
  delay(10);

  ConnectionWifi();

  Wire.begin();
  Wire.beginTransmission(si7021Addr);
  Wire.endTransmission();
  
  pinMode(A0, INPUT);         // http://www.instructables.com/id/ESP8266-ADC-Analog-Sensors/

  wifi_set_sleep_type(LIGHT_SLEEP_T);
}

void loop() {
  dataCapteur();

  Serial.println("Waiting...");  
  WiFi.forceSleepBegin(15000 * 1000);
   
  delay(15000);
    
  // Deep Sleep
  //Serial.println("ESP8266 in sleep mode");
  //ESP.deepSleep(sleepTimeS * 1000000);  
}

void SendData(float h, float t, float temp_f){
  
  while (!client.connect(server,80)) {  //   "184.106.153.149" or api.thingspeak.com
     Serial.print("client not connect   "); 
     Serial.print("status: "); 
     Serial.println(WiFi.status());             // WL_CONNECTED=3 WL_DISCONNECTED=6 WL_NO_SSID_AVAIL=1
     if(WiFi.status() != WL_CONNECTED){
         WiFi.disconnect();
         ConnectionWifi();
     }
     delay(500);
  }
  
  Serial.print("Send...");

    String postStr = apiKey;
           postStr +="&field1=";
           postStr += String(t);
           postStr +="&field2=";
           postStr += String(h);
           postStr += "\r\n\r\n";
 
     client.print("POST /update HTTP/1.1\n"); 
     client.print("Host: api.thingspeak.com\n"); 
     client.print("Connection: close\n"); 
     client.print("X-THINGSPEAKAPIKEY: "+apiKey+"\n"); 
     client.print("Content-Type: application/x-www-form-urlencoded\n"); 
     client.print("Content-Length: "); 
     client.print(postStr.length()); 
     client.print("\n\n"); 
     client.print(postStr);
     
     Serial.print("Temperature: ");
     Serial.print(t);
     Serial.print(" degrees Celcius Humidity: "); 
     Serial.print(h);
     Serial.print(" Analog Read : "); 
     Serial.print(analogRead(A0));
     Serial.println("% send to Thingspeak");  

     client.stop();
}


void dataCapteur(){
  
  unsigned int data[2];
 
  Wire.beginTransmission(si7021Addr);
  //Send humidity measurement command
  Wire.write(0xF5);
  Wire.endTransmission();
  delay(500);
 
  // Request 2 bytes of data
  Wire.requestFrom(si7021Addr, 2);
  // Read 2 bytes of data to get humidity
  if(Wire.available() == 2)
  {
    data[0] = Wire.read();
    data[1] = Wire.read();
  }
 
  // Convert the data
  float humidity  = ((data[0] * 256.0) + data[1]);
  humidity = ((125 * humidity) / 65536.0) - 6;
 
  Wire.beginTransmission(si7021Addr);
  // Send temperature measurement command
  Wire.write(0xF3);
  Wire.endTransmission();
  delay(500);
 
  // Request 2 bytes of data
  Wire.requestFrom(si7021Addr, 2);
 
  // Read 2 bytes of data for temperature
  if(Wire.available() == 2)
  {
    data[0] = Wire.read();
    data[1] = Wire.read();
  }
 
  // Convert the data
  float temp  = ((data[0] * 256.0) + data[1]);
  float celsTemp = ((175.72 * temp) / 65536.0) - 46.85;
  float fahrTemp = celsTemp * 1.8 + 32;
 
  // Output data to serial monitor
  Serial.print("Humidity : ");
  Serial.print(humidity);
  Serial.println(" % RH");
  Serial.print("Celsius : ");
  Serial.print(celsTemp);
  Serial.println(" C");
  Serial.print("Fahrenheit : ");
  Serial.print(fahrTemp);
  Serial.println(" F");

  SendData(humidity, celsTemp, fahrTemp);
}

